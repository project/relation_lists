<?php
/**
 * @file
 * Definition of views_handler_filter_relation_list_target_bundles.
 */

/**
 * Class views_handler_filter_relation_list_target_bundles.
 *
 * Views filter handler that allows to filter list of relation lists with
 * entity types that relation list can contain.
 */
class views_handler_filter_relation_list_target_bundles extends views_handler_filter_many_to_one {

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'select',
      '#options' => $this->get_value_options(),
      '#size' => 15,
      '#multiple' => TRUE,
      '#title' => empty($form_state['exposed']) ? t('Value') : '',
      '#default_value' => empty($form_state['exposed']) ? $this->value : array(),
    );
  }

  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_options = relation_list_get_allowed_target_bundles_options(FALSE);
    }

    return $this->value_options;
  }

  function admin_summary() {
    if ($this->is_a_group()) {
      return t('grouped');
    }
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
    $info = $this->operators();

    $this->get_value_options();

    if (!is_array($this->value)) {
      return NULL;
    }

    $operator = check_plain($info[$this->operator]['short']);
    $values = '';
    if (in_array($this->operator, $this->operator_values(1))) {
      // Choose different kind of output for 0, a single and multiple values.
      if (count($this->value) == 0) {
        $values = t('Unknown');
      }
      elseif (count($this->value) == 1) {
        // If any, use the 'single' short name of the operator instead.
        if (isset($info[$this->operator]['short_single'])) {
          $operator = check_plain($info[$this->operator]['short_single']);
        }
        $target = array_shift($this->value);
        $values = relation_list_get_target_label($target);
      }
      else {
        foreach ($this->value as $value) {
          if ($values !== '') {
            $values .= ', ';
          }
          if (drupal_strlen($values) > 8) {
            $values .= '...';
            break;
          }
          $values .= relation_list_get_target_label($value);
        }
      }
    }

    return $operator . (($values !== '') ? ' ' . $values : '');
  }

  function query() {
    if (empty($this->value)) {
      return;
    }
    else {
      $table = 'relation_list_target_bundles';
      $join = new views_join();
      $join->table = $table;
      $join->field = 'rlid';
      $join->left_table = 'relation_list';
      $join->left_field = 'rlid';
      $join->type = 'LEFT';
      $alias = $this->query->add_table($table, NULL, $join);
      if (count($this->value) > 1) {
        $this->query->set_where_group('OR', $this->options['group']);
        foreach ($this->value as $target) {
          list($entity_type, $bundle) = explode(':', $target);
          // Don't using arguments substitution here because somehow
          // args of all expression add here get overridden by args of
          // first expression.
          $snippet = "$alias.entity_type IN ('$entity_type', '*')";
          if ($bundle != '*') {
            $snippet .= " AND $alias.bundle IN ('$bundle', '*')";
          }
          $this->query->add_where_expression($this->options['group'], $snippet);
        }
      }
      else {
        $target = $this->value[0];
        list($entity_type, $bundle) = explode(':', $target);
        $this->query->add_where($this->options['group'], $alias . '.entity_type', array($entity_type, '*'), 'IN');
        if ($bundle != '*') {
          $this->query->add_where($this->options['group'], $alias . '.bundle', array($bundle, '*'), 'IN');
        }
      }
      $this->query->set_distinct();
    }
  }
}
