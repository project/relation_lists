<?php
/**
 * @file
 * Definition of views_handler_field_relation_list_link.
 */

/**
 * Field handler to present a link to the relation list.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_relation_list_link extends views_handler_field_entity {

  function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
    parent::options_form($form, $form_state);

    // The path is set by render_link function so don't allow to set it.
    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }

  function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_link($entity, $values);
    }
  }

  function render_link($relation_list, $values) {
    // Ensure user has access to this relation.
    if (!relation_list_access('view', $relation_list)) {
      return NULL;
    }

    $this->options['alter']['make_link'] = TRUE;
    $relation_list_uri = entity_uri('relation_list', $relation_list);
    $this->options['alter']['path'] = $relation_list_uri['path'];
    $text = !empty($this->options['text']) ? $this->options['text'] : t('View');

    return $text;
  }
}
