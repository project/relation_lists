<?php
/**
 * @file
 * Definition of views_handler_field_relation_list_item_title.
 */

/**
 * Field handler to present a title of the relation list item.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_relation_list_item_title extends views_handler_field {

  function query() {
    // This field is computed.
    unset($this->real_field);
    unset($this->field);
    $this->ensure_my_table();
    $this->add_additional_fields(array('entity_type' => 'entity_type', 'entity_id' => 'entity_id'));
  }


  function render($values) {
    $entity_type = $values->{$this->aliases['entity_type']};
    $entity_id = $values->{$this->aliases['entity_id']};
    $entity = entity_load_single($entity_type, $entity_id);


    return entity_label($entity_type, $entity);
  }
}
