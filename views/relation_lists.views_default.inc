<?php
/**
 * @file
 * Default views definitions.
 */

/**
 * Implements hook_views_default_views().
 *
 * Defines administrative screen displayed at admin/structure/relation-lists.
 */
function relation_lists_views_default_views() {

  $view = new view();
  $view->name = 'relation_lists';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'relation_list';
  $view->human_name = 'Relation lists';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Relation lists';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'views_bulk_operations' => 'views_bulk_operations',
    'title' => 'title',
    'private' => 'private',
    'delete' => 'delete',
    'edit' => 'edit',
    'link' => 'link',
    'name' => 'name',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'private' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delete' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'link' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = 'Displaying @start - @end of @total lists.';
  /* Relationship: Relation list: Owner */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'relation_list';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['label'] = 'Owner';
  $handler->display->display_options['relationships']['uid']['required'] = TRUE;
  /* Field: Bulk operations: Relation list */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'relation_list';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '100';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 1,
      'label' => 'Delete list',
    ),
    'action::views_bulk_operations_delete_revision' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Relation list: Label */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'relation_list';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Title';
  /* Field: Relation list: Private */
  $handler->display->display_options['fields']['private']['id'] = 'private';
  $handler->display->display_options['fields']['private']['table'] = 'relation_list';
  $handler->display->display_options['fields']['private']['field'] = 'private';
  $handler->display->display_options['fields']['private']['label'] = 'Type';
  $handler->display->display_options['fields']['private']['type'] = 'custom';
  $handler->display->display_options['fields']['private']['type_custom_true'] = 'Private';
  $handler->display->display_options['fields']['private']['type_custom_false'] = 'Public';
  $handler->display->display_options['fields']['private']['not'] = 0;
  /* Field: Relation list: Delete link */
  $handler->display->display_options['fields']['delete']['id'] = 'delete';
  $handler->display->display_options['fields']['delete']['table'] = 'relation_list';
  $handler->display->display_options['fields']['delete']['field'] = 'delete';
  $handler->display->display_options['fields']['delete']['label'] = 'Delete';
  $handler->display->display_options['fields']['delete']['exclude'] = TRUE;
  $handler->display->display_options['fields']['delete']['element_label_colon'] = FALSE;
  /* Field: Relation list: Edit link */
  $handler->display->display_options['fields']['edit']['id'] = 'edit';
  $handler->display->display_options['fields']['edit']['table'] = 'relation_list';
  $handler->display->display_options['fields']['edit']['field'] = 'edit';
  $handler->display->display_options['fields']['edit']['label'] = '';
  $handler->display->display_options['fields']['edit']['exclude'] = TRUE;
  $handler->display->display_options['fields']['edit']['element_label_colon'] = FALSE;
  /* Field: Relation list: Link */
  $handler->display->display_options['fields']['link']['id'] = 'link';
  $handler->display->display_options['fields']['link']['table'] = 'relation_list';
  $handler->display->display_options['fields']['link']['field'] = 'link';
  $handler->display->display_options['fields']['link']['label'] = '';
  $handler->display->display_options['fields']['link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['link']['element_label_colon'] = FALSE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Owner';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[edit] | [delete]';
  // @todo: This is temporary hidden.
  //  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[link] | [edit] | [delete]';
  /* Filter criterion: Relation list: Private */
  $handler->display->display_options['filters']['private']['id'] = 'private';
  $handler->display->display_options['filters']['private']['table'] = 'relation_list';
  $handler->display->display_options['filters']['private']['field'] = 'private';
  $handler->display->display_options['filters']['private']['value'] = 'All';
  $handler->display->display_options['filters']['private']['group'] = 1;
  $handler->display->display_options['filters']['private']['exposed'] = TRUE;
  $handler->display->display_options['filters']['private']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['private']['expose']['label'] = 'List type';
  $handler->display->display_options['filters']['private']['expose']['operator'] = 'private_op';
  $handler->display->display_options['filters']['private']['expose']['identifier'] = 'private';
  $handler->display->display_options['filters']['private']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Relation list: Target bundles */
  $handler->display->display_options['filters']['target_bundles']['id'] = 'target_bundles';
  $handler->display->display_options['filters']['target_bundles']['table'] = 'relation_list';
  $handler->display->display_options['filters']['target_bundles']['field'] = 'target_bundles';
  $handler->display->display_options['filters']['target_bundles']['exposed'] = TRUE;
  $handler->display->display_options['filters']['target_bundles']['expose']['operator_id'] = 'target_bundles_op';
  $handler->display->display_options['filters']['target_bundles']['expose']['label'] = 'Entity type';
  $handler->display->display_options['filters']['target_bundles']['expose']['operator'] = 'target_bundles_op';
  $handler->display->display_options['filters']['target_bundles']['expose']['identifier'] = 'target_bundles';
  $handler->display->display_options['filters']['target_bundles']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: User: Name (raw) */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'users';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'uid';
  $handler->display->display_options['filters']['name']['operator'] = 'contains';
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Owner';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Relation list: Tags */
  $handler->display->display_options['filters']['tags']['id'] = 'tags';
  $handler->display->display_options['filters']['tags']['table'] = 'relation_list';
  $handler->display->display_options['filters']['tags']['field'] = 'tags';
  $handler->display->display_options['filters']['tags']['exposed'] = TRUE;
  $handler->display->display_options['filters']['tags']['expose']['operator_id'] = 'tags_op';
  $handler->display->display_options['filters']['tags']['expose']['label'] = 'Tag';
  $handler->display->display_options['filters']['tags']['expose']['operator'] = 'tags_op';
  $handler->display->display_options['filters']['tags']['expose']['identifier'] = 'tags';
  $handler->display->display_options['filters']['tags']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  $views[$view->name] = $view;

  $view = new view();
  $view->name = 'list_items';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'relation_list_item';
  $view->human_name = 'List items';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'list_item_id' => 'list_item_id',
    'entity_title' => 'entity_title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'list_item_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'entity_title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Relation list item: List item ID */
  $handler->display->display_options['fields']['list_item_id']['id'] = 'list_item_id';
  $handler->display->display_options['fields']['list_item_id']['table'] = 'relation_list_item';
  $handler->display->display_options['fields']['list_item_id']['field'] = 'list_item_id';
  /* Field: Relation list item: List item entity ID */
  $handler->display->display_options['fields']['entity_id']['id'] = 'entity_id';
  $handler->display->display_options['fields']['entity_id']['table'] = 'relation_list_item';
  $handler->display->display_options['fields']['entity_id']['field'] = 'entity_id';
  /* Field: Relation list item: List item entity title */
  $handler->display->display_options['fields']['entity_title']['id'] = 'entity_title';
  $handler->display->display_options['fields']['entity_title']['table'] = 'relation_list_item';
  $handler->display->display_options['fields']['entity_title']['field'] = 'entity_title';
  /* Field: Relation list item: List item entity bundle */
  $handler->display->display_options['fields']['entity_bundle']['id'] = 'entity_bundle';
  $handler->display->display_options['fields']['entity_bundle']['table'] = 'relation_list_item';
  $handler->display->display_options['fields']['entity_bundle']['field'] = 'entity_bundle';
  /* Field: Relation list item: List ID */
  $handler->display->display_options['fields']['list_id']['id'] = 'list_id';
  $handler->display->display_options['fields']['list_id']['table'] = 'relation_list_item';
  $handler->display->display_options['fields']['list_id']['field'] = 'list_id';

  $views[$view->name] = $view;

  return $views;
}
