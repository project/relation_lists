<?php
/**
 * @file
 * Definition of views_handler_field_relation_list_link_edit.
 */

/**
 * Field handler to present a link relation edit.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_relation_list_link_edit extends views_handler_field_relation_list_link {

  /**
   * Renders the link.
   */
  function render_link($relation_list, $values) {
    // Ensure user has access to edit this relation.
    if (!entity_access('edit', 'relation_list', relation_list_load($values->rlid))) {
      return NULL;
    }

    $this->options['alter']['make_link'] = TRUE;
    $relation_list_uri = entity_uri('relation_list', $relation_list);
    $this->options['alter']['path'] = $relation_list_uri['path'] . "/edit";
    $this->options['alter']['query'] = drupal_get_destination();
    $text = !empty($this->options['text']) ? $this->options['text'] : t('Edit');

    return $text;
  }
}
