<?php
/**
 * @file
 * Definition of views_handler_field_relation_list_tags.
 */

/**
 * Field handler to present a tags of the relation list item.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_relation_list_tags extends views_handler_field_entity {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $entity = $this->get_value($values);
    if ($entity && !empty($entity->tags)) {
      return implode(', ', $entity->tags);
    }
  }
}
