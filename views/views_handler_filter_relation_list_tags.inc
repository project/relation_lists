<?php
/**
 * @file
 * Definition of views_handler_filter_relation_list_tags.
 */

/**
 * Class views_handler_filter_relation_list_tags.
 *
 * Views filter handler that allows to filter list of relation lists with tags.
 */
class views_handler_filter_relation_list_tags extends views_handler_filter_many_to_one {

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'select',
      '#options' => $this->get_value_options(),
      '#title' => empty($form_state['exposed']) ? t('Tag') : '',
      '#default_value' => empty($form_state['exposed']) ? $this->value : array(),
      '#size' => 10,
      '#multiple' => TRUE,
    );
  }

  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_options = relation_lists_get_tags_options();
    }

    return $this->value_options;
  }

  function query() {
    if (empty($this->value)) {
      return;
    }
    else {
      $table = 'relation_list_tags';
      $join = new views_join();
      $join->table = $table;
      $join->field = 'id';
      $join->left_table = 'relation_list';
      $join->left_field = 'rlid';
      $join->type = 'LEFT';
      $alias = $this->query->add_table($table, NULL, $join);
      $tag = array_shift($this->value);
      $this->query->add_where($this->options['group'], $alias . '.tag', $tag);
      $this->query->set_distinct();
    }
  }
}
