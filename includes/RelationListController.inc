<?php
/**
 * @file
 * RelationListController class file.
 */

/**
 * Class RelationListController.
 *
 * Relation list entity controller.
 */
class RelationListController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'description' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
      'module' => '',
      'private' => FALSE,
      'target_bundles' => array(),
    );

    return parent::create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    // Make description themed like default fields.
    $content['description'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Description'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => $view_mode,
      '#language' => $langcode,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#entity_type' => 'example_task',
      '#bundle' => 'relation_list',
      '#items' => array(array('value' => $entity->description)),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->description)),
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Load relation lists by list item.
   *
   * @param string $entity_type
   *   Entity type of list item.
   * @param string $entity_id
   *   Entity id of list item.
   *
   * @return array
   *   An array of Relation List entity objects indexed by their ids.
   */
  public function loadByItem($entity_type, $entity_id) {
    $relation_ids = array_keys(
      relation_query($entity_type, $entity_id, 1)
        ->entityCondition('bundle', 'relation_lists_list')
        ->execute()
    );
    $relations = relation_load_multiple($relation_ids);

    $rlids = array();
    foreach ($relations as $relation) {
      $rlids[] = $relation->endpoints[LANGUAGE_NONE][0]['entity_id'];
    }

    return $this->load($rlids);
  }

  /**
   * Loads target bundles.
   *
   * @param array $ids
   *   An array of entity IDs.
   * @param array $conditions
   *   Conditions.
   *
   * @return array
   *   An array of entity objects indexed by their ids. When no results are
   *   found, an empty array is returned.
   */
  public function load($ids = array(), $conditions = array()) {
    $relation_lists = parent::load($ids);
    foreach ($relation_lists as &$relation_list) {
      $relation_list->targetBundlesLoad();
      $relation_list->tagsLoad();
    }

    return $relation_lists;
  }

  /**
   * Saves target bundles.
   *
   * @param RelationListEntity $relation_list
   *   RelationListEntity to save.
   *
   * @return int
   *   Returns SAVED_NEW or SAVED_UPDATED if successful.
   */
  public function save($relation_list) {
    $status = parent::save($relation_list);

    // Processing target bundles.
    db_delete('relation_list_target_bundles')
      ->condition('rlid', $relation_list->rlid)
      ->execute();

    $query = db_insert('relation_list_target_bundles')
      ->fields(array('rlid', 'entity_type', 'bundle'));

    $relation_list->target_bundles = _relation_lists_expand_bundles($relation_list->target_bundles);
    foreach ($relation_list->target_bundles as $target_bundle) {
      list($entity_type, $bundle) = explode(':', $target_bundle);
      $query->values(array($relation_list->rlid, $entity_type, $bundle));
    }
    $query->execute();

    // Processing tags.
    db_delete('relation_list_tags')
      ->condition('id', $relation_list->rlid)
      ->execute();
    $query = db_insert('relation_list_tags')
      ->fields(array('id', 'tag'));
    // Preventing errors when lists will be created/updated programmatically.
    $relation_list->tags = is_string($relation_list->tags) ? drupal_explode_tags($relation_list->tags) : $relation_list->tags;
    foreach ($relation_list->tags as $tag) {
      $query->values(array($relation_list->rlid, $tag));
    }
    $query->execute();

    return $status;
  }

  /**
   * Delete a relation list and remove all relations referencing the list.
   *
   * @param array $ids
   *   An array of entity IDs.
   */
  public function delete($ids) {
    parent::delete($ids);

    db_delete('relation_list_target_bundles')->condition('rlid', $ids)->execute();
    db_delete('relation_list_tags')->condition('id', $ids)->execute();
  }
}
