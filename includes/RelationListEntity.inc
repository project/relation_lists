<?php
/**
 * @file
 * RelationListEntity class file.
 */

/**
 * Class RelationListEntity.
 */
class RelationListEntity extends Entity {

  /**
   * @var int
   * Relation list ID.
   */
  public $rlid;

  /**
   * @var string
   * The machine-readable name.
   */
  public $machine_name;

  /**
   * @var string
   * Relation list title.
   */
  public $title;

  /**
   * @var string
   * Relation list description.
   */
  public $description;

  /**
   * @var int
   * Relation list created date UNIX timestamp.
   */
  public $created;

  /**
   * @var int
   * Relation list changed date UNIX timestamp.
   */
  public $changed;

  /**
   * @var int
   * Relation list owner UID.
   */
  public $uid;

  /**
   * @var bool
   * Relation list owner module name.
   */
  public $module;

  /**
   * @var bool
   * Relation list private status.
   */
  public $private;

  /**
   * @var array
   * Array of entity bundles that could be part of this list.
   */
  public $target_bundles = array();

  /**
   * @var bool
   * Indicates that target bundles were loaded to avoid extra DB queries.
   */
  public $targets_loaded = FALSE;

  /**
   * @var array
   * Array of tags of this list.
   */
  public $tags = array();

  /**
   * @var bool
   * Indicates that tags were loaded to avoid extra DB queries.
   */
  public $tags_loaded = FALSE;

  /**
   * Creates a new entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array(), $entity_type = NULL) {
    parent::__construct($values, 'relation_list');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultLabel() {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultUri() {
    return array(
      'path' => 'admin/structure/relation-lists/' . $this->identifier(),
    );
  }

  /**
   * Load allowed target bundles.
   */
  public function targetBundlesLoad() {
    if (empty($this->targets_loaded)) {
      $target_bundles = array();
      $targets = db_select('relation_list_target_bundles', 't')
        ->condition('rlid', $this->rlid)
        ->fields('t', array('entity_type', 'bundle'))
        ->execute()
        ->fetchAll(PDO::FETCH_ASSOC);
      foreach ($targets as $target) {
        $target_bundles[$target['entity_type']][$target['bundle']] = $target['bundle'];
      }
      // This is a 'free' list - entity type set to *.
      if (array_key_exists('*', $target_bundles)) {
        $this->target_bundles[] = '*:*';
      }
      else {
        foreach ($target_bundles as $entity_type => $bundles) {
          // All bundles of the entity type.
          if (!empty($target_bundles[$entity_type]['*'])) {
            $this->target_bundles[] = $entity_type . ':*';
          }
          else {
            foreach ($bundles as $bundle) {
              $this->target_bundles[] = $entity_type . ':' . $bundle;
            }
          }
        }
      }
      $this->targets_loaded = TRUE;
    }
  }

  /**
   * Check if an entity is allowed to be related to this list.
   *
   * @param string $entity_type
   *   Entity type of list item.
   * @param string $entity_id
   *   Entity id of list item.
   *
   * @return bool
   *   TRUE if the entity is allowed to be part of the list, FALSE if not.
   */
  public function allowedItem($entity_type, $entity_id) {
    $this->targetBundlesLoad();
    if (in_array('*:*', $this->target_bundles) || in_array($entity_type . ':*', $this->target_bundles)) {
      return TRUE;
    }
    else {
      $entity = entity_load_single($entity_type, $entity_id);
      list(,, $entity_bundle) = entity_extract_ids($entity_type, $entity);
      if (in_array($entity_type . ':' . $entity_bundle, $this->target_bundles)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Check if an entity is already part of this list.
   *
   * @param string $entity_type
   *   Entity type of list item.
   * @param string $entity_id
   *   Entity id of list item.
   *
   * @return bool
   *   TRUE if the entity is already part of the list, FALSE if not.
   */
  public function loadRelationID($entity_type, $entity_id) {
    $entity_rids = relation_query($entity_type, $entity_id, 1)
      ->related('relation_list', $this->rlid, 0)
      ->entityCondition('bundle', 'relation_lists_list')
      ->execute();
    return empty($entity_rids) ? FALSE : $entity_rids;
  }

  /**
   * Add item to the relation list.
   *
   * @param string $entity_type
   *   Entity type of list item.
   * @param string $entity_id
   *   Entity id of list item.
   *
   * @return object|FALSE
   *   New relation object or FALSE if item already belongs to Relation List.
   */
  public function addItem($entity_type, $entity_id) {
    // Check if the entity can be added to relation.
    if (!$this->allowedItem($entity_type, $entity_id)) {
      return FALSE;
    }

    // Check if the entity is already on this list.
    if ($this->loadRelationID($entity_type, $entity_id)) {
      return FALSE;
    }

    // Add the entity to the list.
    $endpoints = array(
      array('entity_type' => 'relation_list', 'entity_id' => $this->rlid),
      array('entity_type' => $entity_type, 'entity_id' => $entity_id),
    );
    $relation = relation_create('relation_lists_list', $endpoints);

    // Trigger rules event.
    if (module_exists('rules')) {
      rules_invoke_event('relation_lists_entity_added', $this, $entity_type, $entity_id);
    }

    relation_save($relation);
    // We need this because relation_endpoint_field_validate() loading an
    // entity before the real field values get stored forcing
    // field_attach_load() to set empty cache for relation_list field.
    // Which in turn leads to empty relation_list field value right after
    // entity get created.
    cache_clear_all("field:$entity_type:$entity_id", 'cache_field');

    return $relation;
  }

  /**
   * Remove item from Relation List.
   *
   * @param string $entity_type
   *   Entity type of list item.
   * @param string $entity_id
   *   Entity id of list item.
   */
  public function removeItem($entity_type, $entity_id) {
    $rids = array_keys(
      relation_query($entity_type, $entity_id, 1)
        ->related('relation_list', $this->rlid, 0)
        ->entityCondition('bundle', 'relation_lists_list')
        ->execute()
    );

    // Trigger rules event.
    if (module_exists('rules')) {
      rules_invoke_event('relation_lists_entity_removed', $this, $entity_type, $entity_id);
    }

    relation_delete_multiple($rids);
  }

  /**
   * Loads tags for current list.
   */
  public function tagsLoad() {
    if (empty($this->tags_loaded)) {
      $this->tags = db_select('relation_list_tags', 't')
        ->condition('id', $this->rlid)
        ->fields('t', array('tag'))
        ->execute()
        ->fetchCol();
      $this->tags_loaded = TRUE;
    }
  }

  /**
   * Returns array of united entities.
   *
   * @param array $rlids
   *   The relation list entity id's to compare.
   *
   * @param array $options
   *   The options array allow filtering by entity and bundle.
   *   E.g.
   *   array('entities' =>
   *     array('node' => array(
   *       'profile', 'contact')),
   *     array('crm_core_contact' => array(
   *       'individual'))));
   *
   * @return array
   *   An array of entity id's, keyed by type.
   *   E.g. array('node' => array(1, 2, 3), 'crm_core_contact' => array(1, 2));
   */
  public function union($rlids, $options = array()) {
    $entities = array();
    $entities_query = relation_lists_entities_query($this->rlid, $options);

    // Use "UNION" operator.
    foreach ($rlids as $rlid) {
      $entities_query->union(relation_lists_entities_query($rlid, $options));
    }

    foreach ($entities_query->execute() as $entity) {
      $entities[$entity->entity_type][] = $entity->entity_id;
    }

    return $entities;
  }

  /**
   * Returns array of entities which exists in all lists.
   *
   * @see RelationListEntity::union()
   */
  public function intersection($rlids, $options = array()) {
    $entities = array();
    $list_count = count($rlids) + 1;
    $entities_query = relation_lists_entities_query($this->rlid, $options);

    // Build query:
    // "SELECT * FROM (
    // ($subquery)
    // UNION ALL
    // ($subquery)
    // GROUP BY entity_type AND entity_id
    // HAVING COUNT(*) >= $list_count"
    //
    // Get entities which exists in all sets.
    $union_query = db_select($entities_query, 'main_query');
    $union_query->fields('main_query');

    foreach ($rlids as $rlid) {
      $union_query->union(relation_lists_entities_query($rlid, $options), 'UNION ALL');
    }

    $query = db_select($union_query, 'general');
    $query->fields('general');
    $query->groupBy('general.entity_id');
    $query->groupBy('general.entity_type');
    $query->having('COUNT(*) >= ' . $list_count);

    foreach ($query->execute() as $entity) {
      $entities[$entity->entity_type][] = $entity->entity_id;
    }

    return $entities;
  }

  /**
   * Returns array of entities which exists only in one list.
   *
   * @see RelationListEntity::union()
   */
  public function complement($rlids, $options = array()) {
    $entities = array();
    $entities_query = relation_lists_entities_query($this->rlid, $options);

    // Build query:
    // "SELECT * FROM (
    // ($subquery)
    // UNION ALL
    // ($subquery)
    // GROUP BY entity_type AND entity_id
    // HAVING COUNT(*) = 1"
    //
    // Get entities which exists only in 1 set.
    $union_query = db_select($entities_query, 'main_query');
    $union_query->fields('main_query');

    foreach ($rlids as $rlid) {
      $union_query->union(relation_lists_entities_query($rlid, $options), 'UNION ALL');
    }

    $query = db_select($union_query, 'general');
    $query->fields('general');
    $query->groupBy('general.entity_id');
    $query->groupBy('general.entity_type');
    $query->having('COUNT(*) = 1');

    foreach ($query->execute() as $entity) {
      $entities[$entity->entity_type][] = $entity->entity_id;
    }

    return $entities;
  }
}
