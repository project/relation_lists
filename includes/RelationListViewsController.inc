<?php
/**
 * @file
 * Extending views support.
 */

/**
 * Class RelationListViewsController.
 *
 * Extends views support fo relation list entity.
 */
class RelationListViewsController extends EntityDefaultViewsController {

  /**
   * {@inheritdoc}
   */
  public function views_data() {
    $data = parent::views_data();

    $data['relation_list']['target_bundles'] = array(
      'title' => t('Target bundles'),
      'help' => t('Array of entity bundles that could be part of this list.'),
      'filter' => array(
        'handler' => 'views_handler_filter_relation_list_target_bundles',
      ),
    );

    // View link.
    $data['relation_list']['link'] = array(
      'title' => t('Link'),
      'help' => t('Provide a simple link to the relation list entity.'),
      'field' => array(
        'handler' => 'views_handler_field_relation_list_link',
        'click sortable' => TRUE,
        'real field' => 'rlid',
        'additional fields' => array(
          'rlid',
        ),
      ),
    );

    // Edit link.
    $data['relation_list']['edit'] = array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the relation list entity.'),
      'field' => array(
        'handler' => 'views_handler_field_relation_list_link_edit',
        'click sortable' => TRUE,
        'real field' => 'rlid',
        'additional fields' => array(
          'rlid',
        ),
      ),
    );

    // Delete link.
    $data['relation_list']['delete'] = array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the relation list entity.'),
      'field' => array(
        'handler' => 'views_handler_field_relation_list_link_delete',
        'click sortable' => TRUE,
        'real field' => 'rlid',
        'additional fields' => array(
          'rlid',
        ),
      ),
    );

    $data['relation_list']['tags'] = array(
      'title' => t('Tags'),
      'help' => t('This list tags.'),
      'filter' => array(
        'handler' => 'views_handler_filter_relation_list_tags',
      ),
      'field' => array(
        'handler' => 'views_handler_field_relation_list_tags',
      ),
    );

    $title = t('Relation list item');

    $data['relation_list_item']['table']['group'] = $title;

    $data['relation_list_item']['table']['base'] = array(
      'field' => 'list_item_id',
      'title' => $title,
      'help' => t('Example table contains example content and can be related to nodes.'),
      'weight' => -10,
    );

    $data['relation_list_item']['list_item_id'] = array(
      'title' => t('List item ID'),
      'help' => t('List item ID, constructed from entity type and entity ID concatenated with colon as separator.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
    );

    $data['relation_list_item']['list_id'] = array(
      'title' => t('List ID'),
      'help' => t('Relation list ID.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    );

    $data['relation_list_item']['entity_id'] = array(
      'title' => t('List item entity ID'),
      'help' => t('List item entity ID.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
    );

    $data['relation_list_item']['entity_type'] = array(
      'title' => t('List item entity type'),
      'help' => t('List item entity type.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
    );

    $data['relation_list_item']['entity_title'] = array(
      'title' => t('List item entity title'),
      'help' => t('List item entity title.'),
      'field' => array(
        'handler' => 'views_handler_field_relation_list_item_title',
        'click sortable' => TRUE,
      ),
    );

    $data['relation_list_item']['entity_bundle'] = array(
      'title' => t('List item entity bundle'),
      'help' => t('List item entity bundle.'),
      'field' => array(
        'handler' => 'views_handler_field_relation_list_item_bundle',
        'click sortable' => TRUE,
      ),
    );

    return $data;
  }
}
