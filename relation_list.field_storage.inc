<?php

/**
 * @file
 * Implementation of the relation_lists field storage API.
 *
 * The storage do not support field revisions.
 *
 * ToDo: test languages and revisions.
 */

/**
 * Implements hook_field_storage_info().
 */
function relation_lists_field_storage_info() {
  return array(
    'relation_lists_field_storage' => array(
      'label' => t('Relation Lists field storage'),
      'description' => t('Special field storage for relation_lists field.'),
    ),
  );
}

/**
 * Implements hook_field_storage_create_field().
 */
function relation_lists_field_storage_create_field($field) {
  // Check is all systems ready to operate with relation entities, otherwise
  // throw exception.
  $relation_type = relation_get_types(array('relation_lists_list'));
  if (empty($relation_type)) {
    throw new Exception(t('Required relation type "relation_lists_list" does not exist.'));
  }
}

/**
 * Implements hook_field_storage_load().
 */
function relation_lists_field_storage_load($entity_type, $entities, $age, $fields, $options) {
  // In a case when we have several relation_lists fields in same entity we
  // should sort loaded relations between them by allowed relation
  // lists/entities in field settings but we may have a problems in a case when
  // the fields settings been changed.
  // ToDo: handle a several fields for single entity.
  // ToDo: add revisions support.
  // ToDo: add languages support.
  // ToDo: see if we can optimize permormance here by loding all we need in
  // a less number of queries.

  $entity_info = entity_get_info($entity_type);
  foreach ($fields as $field_id => $ids) {
    $field = field_info_field_by_id($field_id);
    $field_name = $field['field_name'];
    foreach ($entities as $entity) {
      $entity_id = $entity->{$entity_info['entity keys']['id']};
      $existed_rids = array_keys(
        relation_query($entity_type, $entity_id, 1)
          ->entityCondition('bundle', 'relation_lists_list')
          ->execute()
      );
      $relations = relation_load_multiple($existed_rids);
      if (!empty($relations) && empty($entity->{$field_name}[LANGUAGE_NONE])) {
        $entity->{$field_name}[LANGUAGE_NONE] = array();
      }

      foreach ($relations as $relation) {
        $list_entity_id = $relation->endpoints[LANGUAGE_NONE][0]['entity_id'];
        $entity->{$field_name}[LANGUAGE_NONE][] = array(
          'rlid' => $list_entity_id,
        );
      }
    }
  }

}

/**
 * Implements hook_field_storage_write().
 */
function relation_lists_field_storage_write($entity_type, $entity, $op, $fields) {
  // ToDo: add revisions support.
  // ToDo: test languages support.
  // ToDo: see if we can optimize permormance here.

  $entity_info = entity_get_info($entity_type);
  $entity_id = $entity->{$entity_info['entity keys']['id']};
  foreach ($fields as $field_id) {
    $field = field_info_field_by_id($field_id);
    $field_name = $field['field_name'];

    $all_languages = field_available_languages($entity_type, $field);
    $field_languages = array_intersect($all_languages, array_keys((array) $entity->$field_name));
    foreach ($field_languages as $langcode) {
      $values_to_insert = array();
      $values_to_delete = array();

      $values = (empty($entity->{$field_name}[$langcode])) ? array() : _relation_lists_field_extract_rlids($entity->{$field_name}[$langcode]);

      if ($op == FIELD_STORAGE_UPDATE) {
        $old_values = (empty($entity->original->{$field_name}[$langcode])) ? array() : _relation_lists_field_extract_rlids($entity->original->{$field_name}[$langcode]);
        $values_to_insert = array_diff($values, $old_values);
        $values_to_delete = array_diff($old_values, $values);
      }
      elseif ($op == FIELD_STORAGE_INSERT) {
        $values_to_insert = $values;
      }

      foreach ($values_to_insert as $value) {
        relation_list_add_item($value, $entity_type, $entity_id);
      }

      foreach ($values_to_delete as $value) {
        relation_list_remove_item($value, $entity_type, $entity_id);
      }
    }
  }
}

/**
 * Implements hook_field_storage_query().
 *
 * Execute an EntityFieldQuery.
 *
 * ToDo: Right now only one single field storage query may be used. That means you can`t select now en entity both by relation field value
 * and any other field value. We need to solve this.
 */
function relation_lists_field_storage_query(EntityFieldQuery $query) {
  // Build subquery selecting relation revision id`s having queried relation list id as endpoint.
  foreach ($query->fields as $key => $field) {
    if ($field['storage']['type'] == 'relation_lists_field_storage' && !empty($query->fieldConditions[$key]['value']) && !empty($query->fieldConditions[$key]['operator'])) {
      if (isset($sub_query)) {
        $sub_query->condition(
          'endpoints.endpoints_entity_id',
          $query->fieldConditions[$key]['value'],
          $query->fieldConditions[$key]['operator']
        );
      }
      else {
        $sub_query = db_select('field_revision_endpoints', 'endpoints');
        $sub_query->addJoin('RIGHT', 'relation_revision', 'rel_rev', 'endpoints.entity_id = rel_rev.rid');
        $sub_query->condition('endpoints.endpoints_r_index', 0);
        $sub_query->addField('endpoints', 'revision_id');
        $sub_query->condition(
          'endpoints.endpoints_entity_id',
          $query->fieldConditions[$key]['value'],
          $query->fieldConditions[$key]['operator']
        );
      }
    }
  }

  if (empty($sub_query)) {
    return NULL;
  }

  if (empty($query->entityConditions['entity_type']['value'])) {
    throw new EntityFieldQueryException('Property conditions and orders must have an entity type defined.');
  }

  // Build main query selecting entities of opposite endpoints of relations selected in subquery.
  $entity_type = $query->entityConditions['entity_type']['value'];
  $entity_info = entity_get_info($entity_type);
  $select_query = db_select('field_revision_endpoints', 'endpoints');
  $select_query->addField('endpoints', 'endpoints_entity_id', 'entity_id');
  $select_query->addField('endpoints', 'endpoints_entity_type', 'entity_type');
  $select_query->condition('revision_id', $sub_query, 'IN');
  $select_query->condition('endpoints_r_index', 0, '!=');
  $select_query->addJoin(
    'RIGHT',
    $entity_info['base table'],
    'entity_table',
    'endpoints.endpoints_entity_id = entity_table.' . $entity_info['entity keys']['id']
  );
  $select_query->addField('entity_table', $entity_info['entity keys']['revision'], 'revision_id');
  $select_query->addField('entity_table', $entity_info['entity keys']['bundle'], 'bundle');
  $select_query->condition(
    'endpoints_entity_type',
    $query->entityConditions['entity_type']['value'],
    $query->entityConditions['entity_type']['operator']
  );

  if (!empty($query->entityConditions['bundle'])) {
    $select_query->condition(
      'entity_table.' . $entity_info['entity keys']['bundle'],
      $query->entityConditions['bundle']['value'],
      $query->entityConditions['bundle']['operator']
    );
  }

  if (!empty($query->entityConditions['revision_id'])) {
    $select_query->condition(
      'entity_table.' . $entity_info['entity keys']['revision'],
      $query->entityConditions['revision_id']['value'],
      $query->entityConditions['revision_id']['operator']
    );
  }

  foreach ($query->propertyConditions as $property_condition) {
    $select_query->condition(
      'entity_table.' . $property_condition['column'],
      $property_condition['value'],
      $property_condition['operator']
    );
  }

  // Finish query and return selected entities key attributes.
  return $query->finishQuery($select_query);
}

/**
 * Helper to get items ID.
 */
function _relation_lists_field_extract_rlids($items) {
  $ids = array();
  foreach ($items as $item) {
    if (!empty($item['rlid'])) {
      $ids[] = $item['rlid'];
    }
  }

  return $ids;
}
