<?php
/**
 * @file
 * Implements VBO integration.
 */

/**
 * Implements hook_action_info().
 */
function relation_lists_action_info() {
  return array(
    'relation_lists_add_entity_to_list_action' => array(
      'type' => 'entity',
      'label' => t('Add entity to relation list'),
      'configurable' => TRUE,
      'triggers' => array('any'),
    ),
    'relation_lists_remove_entity_from_list_action' => array(
      'type' => 'entity',
      'label' => t('Remove entity from relation list'),
      'configurable' => TRUE,
      'triggers' => array('any'),
    ),
  );
}

/**
 * Form builder for action relation_lists_add_entity_to_list_action.
 */
function relation_lists_add_entity_to_list_action_form($context, &$form_state) {
  $entity_type = $context['entity_type'];
  $lists = relation_list_get_options_by_entity_type($entity_type, TRUE);

  $form = array();
  $form['rlids'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Add selected entities to following lists'),
    '#options' => $lists,
    '#required' => TRUE,
  );
  $form['list_names'] = array(
    '#type' => 'value',
    '#value' => $lists,
  );

  return $form;
}

/**
 * Form builder for action relation_lists_remove_entity_from_list_action.
 */
function relation_lists_remove_entity_from_list_action_form($context, &$form_state) {
  $entity_type = $context['entity_type'];
  $lists = relation_list_get_options_by_entity_type($entity_type, TRUE);

  $form = array();
  $form['rlids'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Remove selected entities from following lists'),
    '#options' => $lists,
    '#required' => TRUE,
  );
  $form['list_names'] = array(
    '#type' => 'value',
    '#value' => $lists,
  );

  return $form;
}

/**
 * Submit handler for action relation_lists_add_entity_to_list_action.
 */
function relation_lists_add_entity_to_list_action_submit($form, $form_state) {
  $rlids = array_filter($form_state['values']['rlids']);

  return array('rlids' => $rlids, 'lists' => $form_state['values']['list_names']);
}

/**
 * Submit handler for action relation_lists_remove_entity_from_list_action.
 */
function relation_lists_remove_entity_from_list_action_submit($form, $form_state) {
  $rlids = array_filter($form_state['values']['rlids']);

  return array('rlids' => $rlids, 'lists' => $form_state['values']['list_names']);
}

/**
 * Action 'relation_lists_add_entity_to_list_action' callback.
 */
function relation_lists_add_entity_to_list_action($entity, $context) {
  $entity_type = $context['entity_type'];
  $entity_id = entity_id($entity_type, $entity);
  foreach ($context['rlids'] as $rlid) {
    $relation = relation_list_add_item($rlid, $entity_type, $entity_id);
    if ($relation) {
      $_SESSION[__FUNCTION__]['processed'][$rlid][] = $entity_id;
    }
  }

  if ($context['progress']['current'] == $context['progress']['total']) {
    $message = t('No entities were added to any of lists, probably relations already exists or not allowed.');
    if (!empty($_SESSION[__FUNCTION__]['processed'])) {
      $message = '';
      foreach ($_SESSION[__FUNCTION__]['processed'] as $rlid => $entity_ids) {
        $list = relation_list_load($rlid);
        $message .= t("To list @list added entities with following id's: @ids.", array('@list' => $list->title, '@ids' => implode(', ', $entity_ids)));
      }
    }

    unset($_SESSION[__FUNCTION__]);
    drupal_set_message($message);
  }
}

/**
 * Action 'relation_lists_remove_entity_from_list_action' callback.
 */
function relation_lists_remove_entity_from_list_action($entity, $context) {
  $entity_type = $context['entity_type'];
  $entity_id = entity_id($entity_type, $entity);
  foreach ($context['rlids'] as $rlid) {
    relation_list_remove_item($rlid, $entity_type, $entity_id);
  }
}
