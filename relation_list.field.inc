<?php
/**
 * @file
 * Field related code.
 */

/**
 * Implements hook_field_info().
 */
function relation_lists_field_info() {
  return array(
    'relation_list' => array(
      'label' => t('Relation list'),
      'description' => t('Put this entity to specified list.'),
      'default_widget' => 'relation_list_autocomplete',
      'default_formatter' => 'relation_lists_default',
      'storage' => array(
        'type' => 'relation_lists_field_storage',
        'settings' => array(),
        'module' => 'relation_lists',
        'active' => 1,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function relation_lists_field_widget_info() {

  $widgets['relation_list_autocomplete'] = array(
    'label' => t('Autocomplete list widget (tagging)'),
    'description' => t('An autocomplete text field.'),
    'field types' => array('relation_list'),
    'settings' => array(
      'match_operator' => 'CONTAINS',
      'size' => 60,
      'path' => 'relation-lists-autocomplete',
    ),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
    ),
  );

  return $widgets;
}

/**
 * Implements hook_field_settings_form().
 */
function relation_lists_field_settings_form($field, $instance, $has_data) {
  $form = array();
  $entity_type = $instance['entity_type'];
  $settings = $field['settings'];

  $form['list_support'] = array(
    '#type' => 'radios',
    '#title' => t('List support'),
    '#description' => t('Select the lists that will be available to users through this field.'),
    '#default_value' => empty($settings['list_support']) ? 'specific_types' : $settings['list_support'],
    '#options' => array(
      'specific_types' => t('Include lists that support specific entities'),
      'specific_lists' => t('Select the lists that can be used for this field'),
      'specific_tags' => t('Include lists by tag'),
    ),
  );

  $form['target_bundles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Include lists that support the following entity types'),
    '#options' => relation_list_get_allowed_target_bundles_options(),
    '#size' => 15,
    '#default_value' => empty($settings['target_bundles']) ? array($entity_type . ':*') : $settings['target_bundles'],
    '#description' => t('Select the entity types a list must support to be included as an option for this field. All public lists will be selected, along with any private lists the user has access to.'),
    '#states' => array(
      'visible' => array(
        ':input[name="field[settings][list_support]"]' => array('value' => 'specific_types'),
      ),
    ),
    '#element_validate' => array('relation_lists_field_settings_form_target_bundles_validate'),
  );

  $options = relation_list_get_options_by_entity_type($entity_type);

  $form['lists_list'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Include the following lists'),
    '#description' => t('Select the specific lists the users will be able to choose from.'),
    '#default_value' => empty($settings['lists_list']) ? array() : $settings['lists_list'],
    '#options' => $options,
    '#states' => array(
      'visible' => array(
        ':input[name="field[settings][list_support]"]' => array('value' => 'specific_lists'),
      ),
    ),
    '#element_validate' => array('relation_lists_field_settings_form_lists_list_validate'),
  );

  $form['list_tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Include lists with following tags'),
    '#default_value' => !empty($settings['list_tags']) ? $settings['list_tags'] : '',
    '#autocomplete_path' => 'relation-lists-tags-autocomplete',
    '#description' => t('Select which tags a list needs to have to be exposed in the field. Separate multiple tags with commas.'),
    '#element_validate' => array('_relation_lists_validate_tags', 'relation_lists_field_settings_form_list_tags_validate'),
    // Maximum tag length is 255 and we expect not more then 5 tags per list.
    '#maxlength' => 255 * 5,
    '#states' => array(
      'visible' => array(
        ':input[name="field[settings][list_support]"]' => array('value' => 'specific_tags'),
      ),
    ),
  );

  $form['list_access'] = array(
    '#title' => t('List access settings'),
    '#type' => 'checkboxes',
    '#description' => t('What kinds of lists can be used with this field?'),
    '#options' => array(
      'global' => t('Global - available throughout the site, for anyone with the global list access permission'),
      'private' => t('Private - created by users with the private list permission'),
      'shared' => t('Shared - list created by another user and shared with the current user. They are available to anyone with the shared list permission'),
    ),
    '#default_value' => isset($settings['list_access']) ? $settings['list_access'] : array('global'),
  );

  $form['new_list'] = array(
    '#title' => t('New list settings'),
    '#description' => t('Indicates whether or not users can create new lists using this field.'),
    '#type' => 'radios',
    '#options' => array(
      'existing' => t('Users can select from lists that already exists.'),
      'new' => t('Users can create new lists using this field.'),
    ),
    '#default_value' => empty($settings['new_list']) ? 'existing' : $settings['new_list'],
  );

  $states = array(
    'visible' => array(
      ':input[name="field[settings][new_list]"]' => array('value' => 'new'),
    ),
  );
  $form['list_targets'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entities that can be added to new lists'),
    '#options' => relation_list_get_allowed_target_bundles_options(),
    '#default_value' => empty($settings['list_targets']) ? array($entity_type . ':*') : $settings['list_targets'],
    '#multiple' => TRUE,
    '#description' => t('Select the types of entities that can be added to new lists created through this field.'),
    '#states' => $states,
    '#element_validate' => array('relation_lists_field_settings_form_list_targets_validate'),
  );

  $form['private'] = array(
    '#type' => 'radios',
    '#title' => t('New lists created through this field are global or private.'),
    '#default_value' => empty($settings['private']) ? 'public' : $settings['private'],
    '#options' => array(
      'public' => t('Global'),
      'private' => t('Private'),
    ),
    '#states' => $states,
  );

  $form['tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Default tags'),
    '#default_value' => !empty($settings['tags']) ? $settings['tags'] : '',
    '#autocomplete_path' => 'relation-lists-tags-autocomplete',
    '#description' => t('Set default tags for new relations lists. Separate multiple tags with commas.'),
    '#element_validate' => array('_relation_lists_validate_tags'),
    // Maximum tag length is 255 and we expect not more then 5 tags per list.
    '#maxlength' => 255 * 5,
    '#states' => $states,
  );

  return $form;
}

/**
 * Implements hook_options_list().
 */
function relation_lists_options_list($field, $instance, $entity_type, $entity) {
  global $user;
  $options = array();

  $query = _relation_lists_options_list_query($field);
  $lists = $query->execute();

  foreach ($lists as $list) {
    // If list is shared for current user and is hidden in field configuration.
    if (!$field['settings']['list_access']['shared'] && $list->private && !relation_lists_is_list_owner($list, $user) && relation_lists_is_list_participant($list, $user)) {
      continue;
    }

    // If list is private for current user and is hidden in field configuration.
    if (!$field['settings']['list_access']['private'] && $list->private && relation_lists_is_list_owner($list, $user)) {
      continue;
    }

    // Check access to list.
    if (relation_list_access('view', $list, $user)) {
      $options[$list->rlid] = $list->title;
    }
  }

  return $options;
}

/**
 * Builds query to search for lists based on field settings.
 *
 * @param array $field
 *   Field info array.
 *
 * @return SelectQuery
 *   Ready to be executed query.
 */
function _relation_lists_options_list_query($field) {
  $or = db_or();
  $settings = $field['settings'];
  $query = db_select('relation_list', 'l')
    ->fields('l', array('rlid', 'title', 'uid', 'private'))
    ->orderBy('title');

  switch ($settings['list_support']) {
    case 'specific_types':
      $types = array_filter($settings['target_bundles']);
      $target_types = array();
      foreach ($types as $type) {
        list($target_entity_type) = explode(':', $type);
        $target_types[] = $target_entity_type;
      }
      $query->join('relation_list_target_bundles', 't', 'l.rlid = t.rlid');
      $or->condition(
        db_and()
          ->condition('t.entity_type', $target_types)
      );

      break;

    case 'specific_lists':
      $specific_lists = array();
      foreach ($settings['lists_list'] as $specific_list => $enabled) {
        if ($enabled !== 0) {
          $specific_lists[$specific_list] = $specific_list;
        }
      }
      $or->condition('rlid', $specific_lists);
      break;

    case 'specific_tags':
      $tags = drupal_explode_tags($settings['list_tags']);
      $query->join('relation_list_tags', 't', 'l.rlid = t.id');
      $or->condition(
        db_and()
          ->condition('t.tag', $tags)
      );

      break;
  }

  // Add condition relative to the field configuration.
  $type_or = db_or();
  // Show private.
  if ($settings['list_access']['private']) {
    $type_or->condition(
      db_and()
        ->condition('private', 1)
    );
  }

  // Show global.
  if ($settings['list_access']['global']) {
    $type_or->condition(
      db_and()
        ->condition('private', 0)
    );
  }

  // Show shared.
  if ($settings['list_access']['shared']) {
    $type_or->condition(
      db_and()
        ->condition('private', 1)
    );
  }

  if ($or->count()) {
    $query->condition($or);
  }

  if ($type_or->count()) {
    $query->condition($type_or);
  }

  return $query;
}

/**
 * Implements hook_field_widget_info_alter().
 */
function relation_lists_field_widget_info_alter(&$info) {
  if (module_exists('options')) {
    $info['options_select']['field types'][] = 'relation_list';
    $info['options_buttons']['field types'][] = 'relation_list';
  }
}

/**
 * Validation handler - checks that at least one entity type is selected.
 */
function relation_lists_field_settings_form_target_bundles_validate($element, &$form_state) {
  if ($form_state['values']['field']['settings']['list_support'] == 'specific_types' && empty($element['#value'])) {
    form_error($element, t('You should select at least one entity type a list must support to be included as an option for this field.'));
  }
}

/**
 * Validation handler - checks that at least one relation list is selected.
 */
function relation_lists_field_settings_form_lists_list_validate($element, &$form_state) {
  if ($form_state['values']['field']['settings']['list_support'] == 'specific_lists' && empty($element['#value'])) {
    form_error($element, t('Please select at least one list.'));
  }
}

/**
 * Validation handler - checks that at least one relation list is selected.
 */
function relation_lists_field_settings_form_list_tags_validate($element, &$form_state) {
  if ($form_state['values']['field']['settings']['list_support'] == 'specific_tags' && empty($element['#value'])) {
    form_error($element, t('Please select at least one tag.'));
  }
}

/**
 * Validation handler - checks that at least one entity type is selected.
 */
function relation_lists_field_settings_form_list_targets_validate($element, &$form_state) {
  if ($form_state['values']['field']['settings']['new_list'] == 'new' && empty($element['#value'])) {
    form_error($element, t('You should select at least one entity type that can be added to new lists created through this field.'));
  }
}

/**
 * Implements hook_field_widget_form().
 */
function relation_lists_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  if ($instance['widget']['type'] == 'relation_list_autocomplete') {
    $entity_type = 'relation_list';
    $entity_ids = array();
    $entity_labels = array();

    // Build an array of entities ID.
    if (empty($items) && !empty($instance['default_value'])) {
      foreach ($instance['default_value'] as $item) {
        $entity_ids[] = $item['rlid'];
      }
    }
    else {
      foreach ($items as $item) {
        $entity_ids[] = $item['rlid'];
      }
    }

    // Load those entities and loop through them to extract their labels.
    $entities = entity_load($entity_type, $entity_ids);

    foreach ($entities as $entity_id => $entity_item) {
      $label = entity_label($entity_type, $entity_item);
      $key = "$label ($entity_id)";
      // Labels containing commas or quotes must be wrapped in quotes.
      if (strpos($key, ',') !== FALSE || strpos($key, '"') !== FALSE) {
        $key = '"' . str_replace('"', '""', $key) . '"';
      }
      $entity_labels[] = $key;
    }

    // Prepare the autocomplete path.
    $autocomplete_path = $instance['widget']['settings']['path'];
    $autocomplete_path .= '/' . $field['field_name'] . '/' . $instance['entity_type'] . '/' . $instance['bundle'];

    $element += array(
      '#type' => 'textfield',
      '#maxlength' => 1024,
      '#default_value' => implode(', ', $entity_labels),
      '#autocomplete_path' => $autocomplete_path,
      '#size' => $instance['widget']['settings']['size'],
      '#element_validate' => array('_relation_list_autocomplete_validate'),
    );

    return $element;
  }
}

/**
 * Implements hook_field_widget_error().
 */
function relation_lists_field_widget_error($element, $error) {
  form_error($element, $error['message']);
}

/**
 * Validation handler - set real ID in form state.
 *
 * Also checks if it's allowed to create new lists with this field and
 * set appropriate values.
 *
 * @param array $element
 *   Field widget element to be validated.
 * @param array $form_state
 *   Form state array.
 */
function _relation_list_autocomplete_validate($element, &$form_state) {
  $value = array();
  // If a value was entered into the autocomplete.
  if (!empty($element['#value'])) {
    $entities = drupal_explode_tags($element['#value']);
    foreach ($entities as $entity) {
      // Take "label (entity id)', match the id from parenthesis.
      if (preg_match("/.+\((\d+)\)/", $entity, $matches)) {
        $value[] = array(
          'rlid' => $matches[1],
        );
      }
      else {
        // If list is not exists and it's allowed that lists can be created
        // via this field, lets mark it.
        $field = field_widget_field($element, $form_state);
        if (!empty($field['settings']) && !empty($field['settings']['new_list']) && $field['settings']['new_list'] == 'new') {
          $value[] = array(
            'rlid' => 'autocreate',
            'title' => $entity,
          );
        }
      }
    }
  }
  // Update the value of this element so the field can validate the product IDs.
  form_set_value($element, $value, $form_state);
}

/**
 * Return JSON based on given field, instance and string.
 *
 * @param array $field_name
 *   The field array definition.
 * @param string $entity_type
 *   Entity type.
 * @param string $bundle_name
 *   Entity bundle.
 * @param string $string
 *   The label of the list to query by.
 */
function relation_list_autocomplete_callback($field_name, $entity_type, $bundle_name, $string = '') {
  $matches = array();
  // The user enters a comma-separated list of relation lists.
  // We only autocomplete the last one.
  $lists = drupal_explode_tags($string);
  $list_last = drupal_strtolower(array_pop($lists));
  $field = field_info_field($field_name);
  // If only one value allowed and user tries to enter more - return nothing.
  if ($field['cardinality'] == '1' && count($lists) == 1 && !empty($list_last)) {
    drupal_json_output(NULL);
  }

  if (!empty($list_last)) {
    $query = _relation_lists_options_list_query($field);
    $query->range(0, 10);
    $suggestions = $query->condition('l.title', '%' . db_like($list_last) . '%', 'LIKE')->execute();
    $prefix = count($lists) ? drupal_implode_tags($lists) . ', ' : '';
    foreach ($suggestions as $list) {
      // Check access to list.
      if (relation_list_access('view', $list)) {
        $key = "$list->title ($list->rlid)";
        // Labels containing commas or quotes must be wrapped in quotes.
        if (strpos($key, ',') !== FALSE || strpos($key, '"') !== FALSE) {
          $key = '"' . str_replace('"', '""', $key) . '"';
        }
        $matches[$prefix . $key] = check_plain($key);
      }
    }
  }

  drupal_json_output($matches);
}

/**
 * Implements hook_field_is_empty().
 */
function relation_lists_field_is_empty($item, $field) {
  return empty($item['rlid']);
}

/**
 * Implements hook_field_presave().
 *
 * Creates new relation list if allowed by field settings.
 */
function relation_lists_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $item) {
    if ($item['rlid'] == 'autocreate') {
      $relation_list = entity_create('relation_list', array());
      $relation_list->title = $item['title'];
      $relation_list->target_bundles = $field['settings']['list_targets'];
      $relation_list->private = $field['settings']['private'] == 'private' ? 1 : 0;
      $relation_list->tags = $field['settings']['tags'];
      entity_save('relation_list', $relation_list);
      $items[$delta]['rlid'] = $relation_list->rlid;
    }
  }
}

/**
 * Implements hook_field_create_field().
 *
 * Switch storage mechanism for 'relation_list' fields.
 * Drupal doesn't have API method to assign a field storage for individual
 * field, so we will make a workaround:
 * - withdraw changes made by field_sql_storage_field_storage_create_field()
 *   (it is already been called).
 * - run hook_field_storage_create_field() implemented by field storage we are
 *   switching on.
 * - update field settings in field_config table.
 */
function relation_lists_field_create_field($field) {
  if ($field['type'] == 'relation_list') {
    // First, delete tables created for field by field_sql_storage.
    $schema = _field_sql_storage_schema($field);
    $table_names = array_keys($schema);
    foreach ($table_names as $name) {
      db_drop_table($name);
    }

    // Update field storage settings.
    $field['storage'] = array(
      'type' => 'relation_lists_field_storage',
      'settings' => array(),
      'module' => 'relation_lists',
      'active' => 1,
    );

    try {
      // Invoke hook_field_storage_create_field() implemented by updated
      // field storage.
      module_invoke($field['storage']['module'], 'field_storage_create_field', $field);

      // Update field conf record.
      $record = array(
        'id' => $field['id'],
        'storage_type' => $field['storage']['type'],
        'storage_module' => $field['storage']['module'],
        'storage_active' => $field['storage']['active'],
        'cardinality' => -1,
      );

      // Update the field storage.
      $primary_key = array('id');
      drupal_write_record('field_config', $record, $primary_key);
    }
    catch (Exception $e) {
      // If storage creation failed, remove the field_config record before
      // rethrowing the exception.
      db_delete('field_config')
        ->condition('id', $field['id'])
        ->execute();
      throw $e;
    }
  }
}

/**
 * Get relation lists options for specific entity type.
 *
 * @param string|array $entity_type
 *   Entity type to search lists for.
 * @param bool $private
 *   Select only public lists by default, otherwise return public and private.
 *
 * @return array
 *   Array of list titles keyed by list ID.
 */
function relation_list_get_options_by_entity_type($entity_type, $private = FALSE) {
  $query = db_select('relation_list_target_bundles', 't');
  if ($private === FALSE) {
    $query->condition('private', 0);
  }
  $query->join('relation_list', 'r', 'r.rlid = t.rlid');
  $lists = $query->fields('r', array('rlid', 'title'))
    ->condition('t.entity_type', $entity_type)
    ->orderBy('r.title')
    ->execute()
    ->fetchAllKeyed();

  return $lists;
}

/**
 * Implements hook_field_formatter_info().
 */
function relation_lists_field_formatter_info() {
  return array(
    'relation_lists_default' => array(
      'label' => t('Default'),
      'field types' => array('relation_list'),
    ),
  );
}

/**
 * Implements hook_field_formatter_prepare_view().
 */
function relation_lists_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  $rlids = array();

  // Collect every possible list attached to any of the fieldable entities.
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => $item) {
      // Force the array key to prevent duplicates.
      $rlids[$item['rlid']] = $item['rlid'];
    }
  }

  if (!empty($rlids)) {
    $lists = relation_list_load_multiple($rlids);

    // Iterate through the fieldable entities again to attach the loaded lists.
    foreach ($entities as $id => $entity) {
      $rekey = FALSE;

      foreach ($items[$id] as $delta => $item) {
        // Check whether the list field instance value could be loaded.
        if (isset($lists[$item['rlid']])) {
          // Add list.
          $items[$id][$delta]['relation_list'] = $lists[$item['rlid']];
        }
        // Otherwise, unset the instance value, since the list does not exist.
        else {
          unset($items[$id][$delta]);
          $rekey = TRUE;
        }
      }

      if ($rekey) {
        // Rekey the items array.
        $items[$id] = array_values($items[$id]);
      }
    }
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function relation_lists_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'relation_lists_default':
      $list_items = array();
      foreach ($items as $item) {
        $list_items[] = entity_label('relation_list', $item['relation_list']);
      }

      $element[] = array(
        '#theme' => 'item_list',
        '#items' => $list_items,
        '#type' => 'ul',
      );
      break;
  }

  return $element;
}
