<?php
/**
 * @file
 * Hooks provided by the Relation lists module.
 */

/**
 * Deny or allow access to entity CRUD before any other access check.
 *
 * Modules implementing this hook can return FALSE to provide a blanket
 * prevention for the user to perform the requested operation on the specified
 * entity. If no modules implementing this hook return FALSE but at least one
 * returns TRUE, then the operation will be allowed, even for a user without
 * role based permission to perform the operation.
 *
 * If no modules return FALSE but none return TRUE either, normal permission
 * based checking will apply.
 *
 * @param string $op
 *   The request operation: view, update, create, or delete.
 * @param object $entity
 *   The entity to perform the operation on.
 * @param object $account
 *   The user account whose access should be determined.
 *
 * @return bool
 *   TRUE or FALSE indicating an explicit denial of permission or a grant in the
 *   presence of no other denials; NULL to not affect the access check at all.
 */
function hook_relation_list_access($op, $entity, $account) {
  // No example.
}
