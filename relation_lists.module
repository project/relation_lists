<?php
/**
 * @file
 * Module file for Relation lists module.
 */

define('RELATION_LISTS_RELATION_TYPE', 'relation_lists_list');
define('RELATION_LISTS_ALL_ALLOWED', '*:*');

require_once 'relation_list.field.inc';
require_once 'relation_list.field_storage.inc';
require_once 'relation_lists.actions.inc';
require_once 'relation_lists.rules.inc';

/**
 * Implements hook_entity_info().
 *
 * Defines relation list entity.
 */
function relation_lists_entity_info() {
  $info = array();

  $info['relation_list'] = array(
    'label' => t('Relation list'),
    'entity class' => 'RelationListEntity',
    'controller class' => 'RelationListController',
    'views controller class' => 'RelationListViewsController',
    'base table' => 'relation_list',
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'access callback' => 'relation_list_access',
    'load hook' => 'relation_list_load',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'rlid',
      'label' => 'title',
    ),
    'view modes' => array(
      'full' => array(
        'label' => t('Full'),
        'custom settings' => FALSE,
      ),
    ),
    'module' => 'relation_lists',
  );


  return $info;
}

/**
 * Implements hook_menu().
 */
function relation_lists_menu() {
  $items = array();

  $relation_list_uri = 'admin/structure/relation-lists/%relation_list';
  $argument_position = 3;

  $items['admin/structure/relation-lists/add'] = array(
    'title' => 'Add Relation list',
    'page callback' => 'relation_list_add_page',
    'access arguments' => array('create relation_lists'),
    'type' => MENU_LOCAL_ACTION,
    'tab_parent' => 'admin/structure/relation-lists',
    'tab_root' => 'admin/structure/relation-lists',
    'file' => 'relation_list.admin.inc',
  );

  $items[$relation_list_uri] = array(
    'title callback' => 'entity_label',
    'title arguments' => array('relation_list', $argument_position),
    'page callback' => 'relation_list_view',
    'page arguments' => array($argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('view', 'relation_list', $argument_position),
  );

  $items[$relation_list_uri . '/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items[$relation_list_uri . '/delete'] = array(
    'title' => 'Delete Relation list',
    'title callback' => 'relation_list_label',
    'title arguments' => array($argument_position),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('relation_list_delete_form', $argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('delete', 'relation_list', $argument_position),
    'file' => 'relation_list.admin.inc',
  );

  $items[$relation_list_uri . '/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('relation_list_form', $argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'relation_list', $argument_position),
    'file' => 'relation_list.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  // We are using separate page callback instead of creating views page display
  // to correctly maintain breadcrumbs.
  $items['admin/structure/relation-lists'] = array(
    'title' => 'Relation lists',
    'description' => 'This page provides administrators with a reference to all the relation lists used in a site.',
    'page callback' => 'relation_lists_overview',
    'access arguments' => array('administer relation_lists'),
    'file' => 'relation_list.admin.inc',
  );

  $items['relation-lists-autocomplete/%/%/%'] = array(
    'title' => 'Relation list autocomplete callback',
    'page callback' => 'relation_list_autocomplete_callback',
    'page arguments' => array(1, 2, 3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['relation-lists-tags-autocomplete'] = array(
    'title' => 'Rules tags autocomplete',
    'page callback' => 'relation_lists_autocomplete_tags',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  if (module_exists('devel')) {
    $items[$relation_list_uri . '/devel'] = array(
      'title' => 'Devel',
      'page callback' => 'relation_list_devel_load_object',
      'page arguments' => array($argument_position),
      'access arguments' => array('access devel information'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 100,
    );
  }

  return $items;
}

/**
 * Load a relation list.
 *
 * @param int $rlid
 *   Relation list ID.
 *
 * @return \RelationListEntity
 *   Relation list object or FALSE.
 * @see relation_list_load_multiple()
 */
function relation_list_load($rlid) {
  return entity_load_single('relation_list', $rlid);
}

/**
 * Load multiple relation lists.
 *
 * @param array $rlids
 *   An array of relation list IDs to be loaded.
 * @param array $conditions
 *   Read entity_load() description.
 * @param bool $reset
 *   Whether to reset the internal cache.
 *
 * @return array
 *   An array of entity object indexed by their ids
 * @see entity_load()
 */
function relation_list_load_multiple($rlids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('relation_list', $rlids, $conditions, $reset);
}

/**
 * Stores relation list entity to database.
 *
 * @param object $relation_list
 *   Relation list object.
 *
 * @return int
 *   Returns status of saved entity SAVED_NEW or SAVED_UPDATED.
 */
function relation_list_save($relation_list) {
  return entity_save('relation_list', $relation_list);
}

/**
 * Deletes relation list entity from database.
 *
 * @param object $relation_list
 *   Relation list object.
 */
function relation_list_delete($relation_list) {
  entity_delete('relation_list', entity_id('relation_list', $relation_list));
}

/**
 * Deletes multiple relation list entities from database.
 *
 * @param array $rlids
 *   Relation list IDs array.
 */
function relation_list_delete_multiple($rlids) {
  entity_delete_multiple('relation_list', $rlids);
}

/**
 * Implements hook_entity_property_info_alter().
 *
 * In case we define hook_entity_property_info() we will drop support of
 * Entity API for properties, which is definitely not what we want, we are
 * using alter to improve entity API properties support.
 */
function relation_lists_entity_property_info_alter(&$info) {
  $properties =& $info['relation_list']['properties'];
  $properties['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the list was created."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer relation_lists',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'schema field' => 'changed',
    'description' => t("The date the list was updated."),
    'setter callback' => 'entity_property_verbatim_set',
  );
  $properties['uid'] = array(
    'label' => t('Owner'),
    'type' => 'user',
    'description' => t("The owner of the list."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer relation_lists',
    'required' => TRUE,
    'schema field' => 'uid',
  );
  $properties['private'] = array(
    'label' => t('Private'),
    'type' => 'boolean',
    'schema field' => 'private',
    'description' => t('If true, only an owner and a selected group of users can see the list.'),
  );
  $properties['target_bundles'] = array(
    'label' => t('Target bundles'),
    'description' => t('Array of entity bundles that could be part of this list.'),
    'type' => 'list',
  );
}

/**
 * Relation list view callback.
 */
function relation_list_view($relation_list) {
  drupal_set_title(entity_label('relation_list', $relation_list));

  return entity_view('relation_list', array(entity_id('relation_list', $relation_list) => $relation_list), 'full');
}

/**
 * Implements hook_ctools_plugin_api().
 */
function relation_lists_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "relation" && $api == "relation_type_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function relation_lists_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'relation_lists') . '/views',
  );
}

/**
 * Get entity bundles allowed to participate in relation lists.
 *
 * @return array
 *   Array of entity bundles allowed to participate in relation lists.
 */
function relation_list_get_allowed_target_bundles() {
  $relation_type = relation_type_load(RELATION_LISTS_RELATION_TYPE);

  return $relation_type->target_bundles;
}

/**
 * Get allowed bundles ready to be used as select options.
 *
 * @param bool $all
 *   Indicates that "All bundles" option should be included.
 *
 * @return array
 *   Array of entity bundles allowed to participate in relation lists.
 */
function relation_list_get_allowed_target_bundles_options($all = TRUE) {
  $options = $all ? array(RELATION_LISTS_ALL_ALLOWED => 'All Entities') : array();
  // It is vital to use allowed targets this way because this allows
  // site admins to remove some entity types from targets with UI.
  $allowed_targets = relation_list_get_allowed_target_bundles();

  foreach ($allowed_targets as $target) {
    list($entity_type) = explode(':', $target);
    $entity_info = entity_get_info($entity_type);
    $options[$target] = check_plain($entity_info['label']);
  }

  return $options;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds autocomplete to user name filter.
 */
function relation_lists_form_views_exposed_form_alter(&$form, $form_state) {
  if ($form['#id'] == 'views-exposed-form-relation-lists-default') {
    $form['name']['#autocomplete_path'] = 'user/autocomplete';
    $form['private']['#options'][1] = t('Private');
    $form['private']['#options'][0] = t('Public');
  }
}

/**
 * Page callback for devel.
 */
function relation_list_devel_load_object($relation_list) {
  module_load_include('inc', 'devel', 'devel.pages');

  return devel_load_object('relation_list', $relation_list, check_plain(entity_label('relation_list', $relation_list)));
}

/**
 * Expand '*' bundles to separate entries.
 *
 * For each "*" values we adding separate ENTITY_TYPE:BUNDLE values
 * so we can easily check matches with array_intersect().
 *
 * @param array $bundles
 *   Array bundles.
 *
 * @return array
 *   Expanded array of bundles.
 */
function _relation_lists_expand_bundles($bundles) {
  $bundles = array_filter($bundles);
  foreach ($bundles as $entry) {
    list($entity_type) = explode(':', $entry);
    // In case we creating a 'free' list(we can put any bundle of any type
    // in it) it's hard to match this in SQL, so we expanding this into
    // all bundles of all entity types.
    if ($entity_type == '*') {
      $entity_info = entity_get_info();
      foreach (array_keys($entity_info) as $entity_type) {
        $bundles[] = $entity_type . ':*';
      }
    }
  }

  return array_unique($bundles);
}

/**
 * Returns label for target bundle.
 *
 * @param string $target
 *   Target bundle in form of ENTITY_TYPE:BUNDLE,
 * where BUNDLE could be '*'.
 *
 * @return string
 *   Label for specified target or FALSE;
 */
function relation_list_get_target_label($target) {
  $label = FALSE;
  list($entity_type, $bundle) = explode(':', $target);
  $entity_info = entity_get_info($entity_type);
  if ($bundle == '*') {
    $label = check_plain('All ' . $entity_info['label'] . ' bundles');
  }
  elseif (isset($entity_info['bundles'][$bundle])) {
    $label = check_plain($entity_info['bundles'][$bundle]['label']);
  }

  return $label;
}

/**
 * Loads relation lists for specific entity type.
 *
 * @param string|array $entity_type
 *   Entity type to search lists for.
 * @param bool $private
 *   Select only public lists by default, otherwise return public and private.
 *
 * @return array
 *   Array of lists keyed by list ID.
 */
function relation_list_load_by_entity_type($entity_type, $private = FALSE) {
  $options = relation_list_get_options_by_entity_type($entity_type, $private);

  return relation_list_load_multiple(array_keys($options));
}

/**
 * Load relation lists by list item.
 *
 * @param string $entity_type
 *   Entity type of list item.
 * @param string $entity_id
 *   Entity id of list item.
 * @param bool $reset
 *   Whether to reset the internal cache.
 *
 * @return array
 *   An array of Relation List entity objects indexed by their ids.
 */
function relation_list_load_by_item($entity_type, $entity_id, $reset = FALSE) {
  if ($reset) {
    entity_get_controller('relation_list')->resetCache();
  }

  return entity_get_controller('relation_list')->loadByItem($entity_type, $entity_id);
}

/**
 * Add item to relation list.
 *
 * @param int $rlid
 *   Relation list ID.
 * @param string $entity_type
 *   Entity type of list item.
 * @param string $entity_id
 *   Entity id of list item.
 *
 * @return object|FALSE
 *   New relation object or FALSE if item already belongs to Relation List.
 */
function relation_list_add_item($rlid, $entity_type, $entity_id) {
  $list_entity = new RelationListEntity(array('rlid' => $rlid));

  return $list_entity->addItem($entity_type, $entity_id);
}

/**
 * Remove item from Relation List.
 *
 * @param int $rlid
 *   Relation list ID.
 * @param string $entity_type
 *   Entity type of list item.
 * @param string $entity_id
 *   Entity id of list item.
 */
function relation_list_remove_item($rlid, $entity_type, $entity_id) {
  $list_entity = new RelationListEntity(array('rlid' => $rlid));
  $list_entity->removeItem($entity_type, $entity_id);
}

/**
 * Implements hook_permission().
 */
function relation_lists_permission() {
  $perms = array(
    'administer relation_lists' => array(
      'title' => t('Administer relation lists'),
      'restrict access' => TRUE,
    ),
    'create relation_lists' => array(
      'title' => t('Create relation lists'),
    ),
    'view private relation lists' => array(
      'title' => t('View private relation lists'),
    ),
    'view global relation lists' => array(
      'title' => t('View global relation lists'),
    ),
    'view shared relation lists' => array(
      'title' => t('View shared relation lists'),
    ),
    'edit private relation lists' => array(
      'title' => t('Edit private relation lists'),
    ),
    'edit global relation lists' => array(
      'title' => t('Edit global relation lists'),
    ),
    'edit shared relation lists' => array(
      'title' => t('Edit shared relation lists'),
    ),
    'delete private relation lists' => array(
      'title' => t('Delete private relation lists'),
    ),
    'delete global relation lists' => array(
      'title' => t('Delete global relation lists'),
    ),
    'delete shared relation lists' => array(
      'title' => t('Delete shared relation lists'),
    ),
  );

  return $perms;
}

/**
 * Determines whether the current user may perform the operation on the node.
 *
 * @param string $op
 *   The operation to be performed on the node. Possible values are:
 *   - "view"
 *   - "edit"
 *   - "delete"
 *   - "create"
 * @param object $relation_list
 *   The relation list object on which the operation is to be performed.
 * @param object $account
 *   Optional, a user object representing the user for whom the operation is to
 *   be performed. Determines access for a user other than the current user.
 *
 * @return bool
 *   TRUE if the operation may be performed, FALSE otherwise.
 */
function relation_list_access($op, $relation_list, $account = NULL) {
  $ops = array('view', 'edit', 'delete', 'create');
  if (!$relation_list || !in_array($op, $ops, TRUE)) {
    // If the $op was not one of the supported ones, we return access denied.
    return FALSE;
  }

  // If no user object is supplied, the access check is for the current user.
  $account = $account ?: $GLOBALS['user'];
  $uid = $account->uid;
  $cid = $relation_list->rlid;
  $rights = &drupal_static(__FUNCTION__, array());

  // If we've already checked access for this list, user and op, return from
  // cache.
  if (isset($rights[$uid][$cid][$op])) {
    return $rights[$uid][$cid][$op];
  }

  $rights[$uid][$cid][$op] = relation_list_access_raw($op, $relation_list, $account);
  return $rights[$uid][$cid][$op];
}

/**
 * Determines whether the current user may perform the operation on the node.
 *
 * @param string $op
 *   The operation to be performed on the node. Possible values are:
 *   - "view"
 *   - "edit"
 *   - "delete"
 *   - "create"
 * @param object $relation_list
 *   The relation list object on which the operation is to be performed.
 * @param object $account
 *   Optional, a user object representing the user for whom the operation is to
 *   be performed. Determines access for a user other than the current user.
 *
 * @return bool
 *   TRUE if the operation may be performed, FALSE otherwise.
 */
function relation_list_access_raw($op, $relation_list, $account = NULL) {
  if (!$relation_list) {
    return FALSE;
  }

  // We grant access to the relation list if both of the following conditions
  // are met:
  // - No modules say to deny access.
  // - At least one module says to grant access.
  // If no module specified either allow or deny, we fall back to the
  // relation list permissions.
  $access = module_invoke_all('relation_list_access', $relation_list, $op, $account);
  if (in_array(FALSE, $access, TRUE)) {
    return FALSE;
  }

  // If user has access to administer mailing lists.
  if (user_access('administer relation_lists', $account)) {
    return TRUE;
  }

  // If user is owner and has access to own entity.
  if (relation_lists_is_list_owner($relation_list, $account) && user_access($op . ' private relation lists', $account)) {
    return TRUE;
  }

  // If list is public and user has access.
  if (!$relation_list->private && user_access($op . ' global relation lists', $account)) {
    return TRUE;
  }

  // If list is private and user has access(in shared).
  if ($relation_list->private && relation_lists_is_list_participant($relation_list, $account) && user_access($op . ' shared relation lists', $account)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Returns whether the user is list owner.
 *
 * @param object $list
 *   Target list.
 *
 * @param bool|object $user
 *   User account.
 *
 * @return bool
 *   TRUE if user has access.
 */
function relation_lists_is_list_owner($list, $user = FALSE) {
  if (!$user) {
    global $user;
  }

  // List is public or user is owner.
  if ($list->uid == $user->uid) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Returns whether the user in shared users.
 *
 * @param object $list
 *   Target list.
 *
 * @param bool|object $user
 *   User account.
 *
 * @return bool
 *   TRUE if user has access.
 */
function relation_lists_is_list_participant($list, $user = FALSE) {
  if (!$user) {
    global $user;
  }

  // Load entity if 'relation_list_users' not present.
  if (!isset($list->relation_list_users)) {
    $list = entity_load_single('relation_list', $list->rlid);
  }

  // When list is private, check if user is in shared users.
  if ($shared_users = field_get_items('relation_list', $list, 'relation_list_users')) {
    foreach ($shared_users as $shared_user) {
      if ($shared_user['target_id'] == $user->uid) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

/**
 * AJAX page callback to load tag suggestions.
 *
 * Largely copied from taxonomy_autocomplete().
 */
function relation_lists_autocomplete_tags($tags_typed = '') {
  // The user enters a comma-separated list of tags. We only autocomplete the
  // last tag.
  $tags_typed = drupal_explode_tags($tags_typed);
  $tag_last = drupal_strtolower(array_pop($tags_typed));

  $tag_matches = array();
  if ($tag_last != '') {
    $query = db_select('relation_list_tags', 'rlt');
    // Do not select already entered terms.
    if (!empty($tags_typed)) {
      $query->condition('rlt.tag', $tags_typed, 'NOT IN');
    }
    // Select rows that match by tag name.
    $tags_return = $query
      ->distinct()
      ->fields('rlt', array('tag'))
      ->condition('rlt.tag', '%' . db_like($tag_last) . '%', 'LIKE')
      ->groupBy('rlt.tag')
      ->range(0, 10)
      ->execute()
      ->fetchCol('rlt.tag');

    $prefix = count($tags_typed) ? drupal_implode_tags($tags_typed) . ', ' : '';

    foreach ($tags_return as $name) {
      $n = $name;
      // Tag names containing commas or quotes must be wrapped in quotes.
      if (strpos($name, ',') !== FALSE || strpos($name, '"') !== FALSE) {
        $n = '"' . str_replace('"', '""', $name) . '"';
      }
      $tag_matches[$prefix . $n] = check_plain($name);
    }
  }

  drupal_json_output($tag_matches);
}

function relation_lists_get_tags_options() {
  $tags =& drupal_static(__FUNCTION__, array());

  if (empty($tags)) {
    $tags = db_select('relation_list_tags', 't')
      ->distinct()
      ->fields('t', array('tag'))
      ->groupBy('tag')
      ->execute()
      ->fetchCol('tag');
  }

  return drupal_map_assoc($tags);
}

/**
 * Validates list tags.
 *
 * Tag cannot be longer than 255 characters.
 */
function _relation_lists_validate_tags($element, &$form_state) {
  if (!empty($element['#value'])) {
    $tags_typed = drupal_explode_tags($element['#value']);
    foreach ($tags_typed as $tag) {
      if (strlen($tag) > 255) {
        form_error($element, t('Looks like one of the entered tags is longer than 255 characters. It should be less.'));
      }
    }
  }
}

/**
 * Load relation list by machine name.
 *
 * @param string $machine_name
 *   Machine name.
 *
 * @return bool|object
 *   Relation list or FALSE on failure.
 */
function relation_list_load_by_name($machine_name) {
  $rlid = db_select('relation_list')
    ->fields('relation_list', array('rlid'))
    ->condition('machine_name', $machine_name)
    ->execute()
    ->fetchField();

  return $rlid ? relation_list_load($rlid) : FALSE;
}

/**
 * Build query for related entities.
 *
 * @param int $rlid
 *   Relation list id.
 *
 * @param array $options
 *   Array of options.
 *
 * @return SelectQuery
 *   SelectQuery of related entities.
 */
function relation_lists_entities_query($rlid, $options = array()) {
  $query = db_select('field_data_endpoints', 'relation');

  // Add entity table.
  $query->leftJoin('field_data_endpoints', 'entity', 'relation.entity_id = entity.entity_id');
  $query->addField('entity', 'endpoints_entity_id', 'entity_id');
  $query->addField('entity', 'endpoints_entity_type', 'entity_type');
  $query->condition('relation.bundle', 'relation_lists_list');
  $query->condition('relation.endpoints_entity_type', 'relation_list');

  // Relation - source, entity - target and filter by rlid.
  $query->condition('relation.delta', 0);
  $query->condition('entity.delta', 1);
  $query->condition('relation.endpoints_entity_id', $rlid);

  // Build bundle conditions.
  $bundle_conditions = db_or();
  if (!empty($options['entities'])) {
    $entity_types = array();
    foreach ($options['entities'] as $entity_type => $bundles) {
      if (is_array($bundles)) {
        $entity_types[] = $entity_type;
        $entity_info = entity_get_info($entity_type);

        // When entity supports bundles.
        if (!empty($entity_info['base table']) && !empty($entity_info['entity keys']['bundle']) && !empty($entity_info['entity keys']['id'])) {
          $table = $entity_info['base table'];
          $bundle_column = $entity_info['entity keys']['bundle'];
          $id_column = $entity_info['entity keys']['id'];

          // Join table based on entity_id and entity_type.
          $query->leftJoin($table, $table, "entity.endpoints_entity_id = {$table}.{$id_column} AND entity.endpoints_entity_type = '{$entity_type}'");

          // Build conditions for bundle.
          $bundle_conditions->condition($table . '.' . $bundle_column, $bundles, 'IN');
        }

      }
      else {
        $entity_types[] = $entity_type;
      }
    }

    // Add filter by entity_type and bundle.
    $query->condition('entity.endpoints_entity_type', $entity_types, 'IN');
    $query->condition($bundle_conditions);
  }

  return $query;
}
