<?php

/**
 * @file
 * Implements the Rules module API for Relation Lists.
 */

/**
 * Implements hook_rules_action_info().
 */
function relation_lists_rules_action_info() {
  return array(
    'relation_lists_entity_add' => array(
      'label' => t('Add entity to a relation list'),
      'group' => t('Relation Lists'),
      'base' => 'relation_lists_rules_entity_add',
      'parameter' => array(
        'entity' => array(
          'type' => 'entity',
          'label' => t('Entity'),
        ),
        'list_parameter' => array(
          'type' => 'relation_list',
          'label' => t('Relation list'),
          'optional' => TRUE,
        ),
        'list_specific' => array(
          'type' => 'list<integer>',
          'label' => t('Relation list'),
          'options list' => 'relation_lists_rules_lists_list',
          'description' => t('Select the available Relation list.'),
          'restriction' => 'input',
          'optional' => TRUE,
        ),
      ),
    ),
    'relation_lists_entity_remove' => array(
      'label' => t('Remove entity from a relation list'),
      'group' => t('Relation Lists'),
      'base' => 'relation_lists_rules_entity_remove',
      'parameter' => array(
        'entity' => array(
          'type' => 'entity',
          'label' => t('Entity'),
        ),
        'list_parameter' => array(
          'type' => 'relation_list',
          'label' => t('Relation list'),
          'optional' => TRUE,
        ),
        'list_specific' => array(
          'type' => 'list<integer>',
          'label' => t('Relation list'),
          'options list' => 'relation_lists_rules_lists_list',
          'description' => t('Select the available Relation list.'),
          'restriction' => 'input',
          'optional' => TRUE,
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 */
function relation_lists_rules_event_info() {
  $items = array(
    'relation_lists_entity_added' => array(
      'label' => t('Entity was added to a relation list'),
      'group' => t('Relation list'),
      'variables' => array(
        'relation_list' => array(
          'type' => 'relation_list',
          'label' => t('Relation list'),
        ),
        'entity_type' => array(
          'type' => 'text',
          'label' => t('Entity type'),
        ),
        'entity_id' => array(
          'type' => 'integer',
          'label' => t('Entity id'),
        ),
      ),
    ),
    'relation_lists_entity_removed' => array(
      'label' => t('Entity was remove from a relation list'),
      'group' => t('Relation list'),
      'variables' => array(
        'relation_list' => array(
          'type' => 'relation_list',
          'label' => t('Relation list'),
        ),
        'entity_type' => array(
          'type' => 'text',
          'label' => t('Entity type'),
        ),
        'entity_id' => array(
          'type' => 'integer',
          'label' => t('Entity id'),
        ),
      ),
    ),
  );

  return $items;
}

/**
 * Lists all available Relation Lists.
 */
function relation_lists_rules_lists_list() {
  $options = array();
  $lists = db_select('relation_list', 'l')
    ->fields('l', array('rlid', 'title', 'uid', 'private'))
    ->orderBy('title')
    ->execute();

  foreach ($lists as $list) {
    // Check access to list.
    if (relation_list_access('view', $list)) {
      $options[$list->rlid] = $list->title;
    }
  }

  return $options;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add validate handler to check selection of relation list on the add form.
 */
function relation_lists_form_rules_ui_add_element_alter(&$form, &$form_state, $form_id) {
  if (!empty($form_state['values']['element_name'])
    && ($form_state['values']['element_name'] == 'relation_lists_entity_add'|| $form_state['values']['element_name'] == 'relation_lists_entity_remove')) {
    $form['#validate'][] = 'relation_lists_rules_list_validate';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add validate handler to check selection of relation list on the edit form.
 */
function relation_lists_form_rules_ui_edit_element_alter(&$form, &$form_state, $form_id) {
  if (!empty($form['parameter'])
    && array_key_exists('entity', $form['parameter'])
    && array_key_exists('list_parameter', $form['parameter'])
    && array_key_exists('list_specific', $form['parameter'])) {
    $form['#validate'][] = 'relation_lists_rules_list_validate';
  }
}

/**
 * Validate callback.
 */
function relation_lists_rules_list_validate(&$form, &$form_state) {
  // At least one relation list should be selected.
  $parameters = $form_state['values']['parameter'];
  if (empty($parameters['list_parameter']['settings']['list_parameter:select']) && empty($parameters['list_specific']['settings']['list_specific'])) {
    form_set_error('', t('At least one relation list should be selected.'));
  }
}

/**
 * Callback for 'relation_lists_entity_add' action.
 */
function relation_lists_rules_entity_add($entity, $list_parameter = NULL, $list_specific = array()) {
  $lists = relation_lists_rules_lists_extract($list_parameter, $list_specific);

  // Add entity to selected lists.
  foreach ($lists as $list) {
    $list->addItem($entity->type(), $entity->getIdentifier());
  }
}

/**
 * Callback for 'relation_lists_entity_remove' action.
 */
function relation_lists_rules_entity_remove($entity, $list_parameter = NULL, $list_specific = array()) {
  $lists = relation_lists_rules_lists_extract($list_parameter, $list_specific);

  // Add entity to selected lists.
  foreach ($lists as $list) {
    $list->removeItem($entity->type(), $entity->getIdentifier());
  }
}

/**
 * Extract lists from arguments.
 */
function relation_lists_rules_lists_extract($list_parameter, $list_specific) {
  $lists = array();

  // Load specific lists.
  if (!empty($list_specific)) {
    $lists = entity_load('relation_list', $list_specific);
  }
  // Add selected list.
  if ($list_parameter) {
    $lists[$list_parameter->rlid] = $list_parameter;
  }

  return $lists;
}
