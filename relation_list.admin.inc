<?php
/**
 * @file
 * Admin pages.
 */

/**
 * Page callback for relation lists overview page.
 *
 * We are using separate page callback instead of creating views page display
 * to correctly maintain breadcrumbs.
 */
function relation_lists_overview() {
  return views_embed_view('relation_lists');
}

/**
 * Relation list add page callback.
 */
function relation_list_add_page() {
  drupal_set_title(t('Create a list'));
  $relation_list = entity_create('relation_list', array());

  return drupal_get_form('relation_list_form', $relation_list);
}

/**
 * Relation list edit form builder.
 */
function relation_list_form($form, &$form_state, $relation_list) {
  $form_state['relation_list'] = $relation_list;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title'),
    '#default_value' => $relation_list->title,
    '#weight' => 1,
  );

  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 255,
    '#machine_name' => array(
      'exists' => 'relation_list_load_by_name',
      'source' => array('title'),
    ),
    '#default_value' => empty($relation_list->machine_name) ? '' : $relation_list->machine_name,
    '#disabled' => !empty($relation_list->machine_name),
    '#weight' => 1.5,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $relation_list->description,
    '#weight' => 2,
  );

  $form['tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Tags'),
    '#default_value' => !empty($relation_list->tags) ? drupal_implode_tags($relation_list->tags) : '',
    '#autocomplete_path' => 'relation-lists-tags-autocomplete',
    '#description' => t('Tags associated with this list, used for filtering in the admin interface. Separate multiple tags with commas.'),
    '#element_validate' => array('_relation_lists_validate_tags'),
    // Maximum tag length is 255 and we expect not more then 5 tags per list.
    '#maxlength' => 255 * 5,
    '#weight' => 2.5,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $relation_list->uid,
  );

  $form['private'] = array(
    '#type' => 'checkbox',
    '#title' => t('Private list?'),
    '#default_value' => $relation_list->private,
    '#weight' => 3,
  );

  $form['target_bundles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entities'),
    '#options' => relation_list_get_allowed_target_bundles_options(),
    '#default_value' => $relation_list->target_bundles,
    '#description' => t('Only the selected entities types can be added to this list.'),
    '#weight' => 5,
  );

  field_attach_form('relation_list', $relation_list, $form, $form_state);

  // Fields elements tuneup.
  $form['relation_list_users']['#weight'] = 4;
  $form['relation_list_users']['#states'] = array(
    'visible' => array(
      ':input[name="private"]' => array('checked' => TRUE),
    ),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  if (entity_access('delete', 'relation_list', $relation_list) && !empty($relation_list->rlid)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('relation_list_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Relation list form submit handler.
 */
function relation_list_form_submit($form, &$form_state) {
  $relation_list = $form_state['relation_list'];
  entity_form_submit_build_entity('relation_list', $relation_list, $form, $form_state);
  relation_list_save($relation_list);
  $form_state['redirect'] = 'admin/structure/relation-lists';
  drupal_set_message(t('List %title saved.', array('%title' => entity_label('relation_list', $relation_list))));
}

/**
 * Delete button submit callback.
 */
function relation_list_form_submit_delete($form, &$form_state) {
  $relation_list = $form_state['relation_list'];
  $relation_list_uri = entity_uri('relation_list', $relation_list);
  $redirect_path = $relation_list_uri['path'] . '/delete';
  $form_state['redirect'] = $redirect_path;
  // We need this to force drupal go to where we want.
  // @see drupal_goto()
  $_GET['destination'] = $redirect_path;
}

/**
 * Delete confirmation form.
 */
function relation_list_delete_form($form, &$form_state, $relation_list) {
  $form_state['relation_list'] = $relation_list;
  $relation_list_uri = entity_uri('relation_list', $relation_list);

  return confirm_form(
    $form,
    t('Are you sure you want to delete relation list %title?', array('%title' => entity_label('relation_list', $relation_list))),
    $relation_list_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function relation_list_delete_form_submit($form, &$form_state) {
  $relation_list = $form_state['relation_list'];
  relation_list_delete($relation_list);
  drupal_set_message(t('Relation list %title has been deleted.', array('%title' => entity_label('relation_list', $relation_list))));
  $form_state['redirect'] = 'admin/structure/relation-lists';
}
