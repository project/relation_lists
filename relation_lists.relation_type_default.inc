<?php
/**
 * @file
 * Default relation type to create relations to list.
 */

/**
 * Implements hook_relation_default_relation_types().
 *
 * Defines relation type that is used to store list of entities
 * allowed to be added to relation lists. By default includes all
 * entity types present in the system, but can be changed via UI.
 */
function relation_lists_relation_default_relation_types() {
  $export = array();

  $bundles = array();
  foreach (array_keys(entity_get_info()) as $entity_type) {
    if ($entity_type == 'relation_list') {
      continue;
    }
    $bundles[] = $entity_type . ':*';
  }

  $relation_type = new stdClass();
  $relation_type->disabled = FALSE;
  $relation_type->in_code_only = TRUE;
  $relation_type->api_version = 1;
  $relation_type->relation_type = RELATION_LISTS_RELATION_TYPE;
  $relation_type->label = 'list of';
  $relation_type->reverse_label = 'in list';
  $relation_type->directional = 1;
  $relation_type->transitive = 0;
  $relation_type->r_unique = 0;
  $relation_type->min_arity = 2;
  $relation_type->max_arity = 2;
  $relation_type->source_bundles = array('relation_list:relation_list');
  $relation_type->target_bundles = $bundles;
  $export[$relation_type->relation_type] = $relation_type;

  return $export;
}
